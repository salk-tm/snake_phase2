# Overview
This is a Snakemake TE4 workflow designed for the FELIX project to assemble and bin genomes using SPAdes and/or Flye.

# Quickstart
### Installing dependencies and running using Singularity
The following script provides some example commands that should setup and execute this snakemake workflow on your read set assuming your reads are in the current directory using singularity
```
# get a test pankmer index
wget https://salk-tm-pub.s3.us-west-2.amazonaws.com/felix/felix.v4.pkidx.tar

# get this pipeline
git clone https://gitlab.com/salk-tm/snake_phase2.git

# pull a container from docker that satisfies dependencies
singularity pull docker://nolanhartwick/salk-containers:felix_assembly

# execute a snakemake command inside the container using your input data
singularity exec --bind ./:/workspace salk-containers_felix_assembly.sif \
    snakemake -p -j 32 --snakefile /workspace/snake_phase2/snake_phase2.smk \
    --config \
        sample=EE01 \
        r1=/workspace/EE01.R1.fastq.gz \
        r2=/workspace/EE01.R2.fastq.gz  \
        ont=/workspace/EE01.ONT.fastq.gz  \
        index=/workspace/felix.v4.pkidx.tar \
```

### Installing dependencies and running using Docker
TODO - Should be very similar to singularity based execution but with docker commands for pulling and executing
```
```

### Installing locally using Conda and apt
Local installation and execution without containerization will require Cython support which requires gcc. If you have apt and conda, the dependency instillation process should go smoothly and should look something like...
```
# get a test pankmer index
wget https://salk-tm-pub.s3.us-west-2.amazonaws.com/felix/felix.v4.pkidx.tar

# clone pipeline repository
git clone https://gitlab.com/salk-tm/snake_phase2.git

# build an environment with needed dependances
wget https://raw.githubusercontent.com/nhartwic/salk-containers/master/felix_assembly.yml
conda env create -n felix_assembly -f felix_assembly.yml
conda activate felix_assembly
apt-get update -y
apt-get install -y build-essential
git clone https://gitlab.com/semarpetrus/pk_binning.git
cd pk_binning/ 
python setup.py build_ext --inplace
python setup.py install
cd ../

# execute a snakemake command to run the workflow on your data
snakemake -p -j 32 \
    --snakefile snake_phase2/snake_phase2.smk \
    --config \
        sample=ecoli \
        r1=illumina_1.fastq.gz \
        r2=illumina_2.fastq.gz \
        ont=nanopore.fastq.gz \
        index=felix.v3.pkidx.tar
```

# Config Fields
| Config | Description |
| ------ | ------ |
| _sample_ | The name of your sample. This sample name will be used as a prefix when naming most of the output files and directories. **REQUIRED** |
| _r1_ | Your Illumina Read 1 (R1) fastq.gz file that will be used for assembly. **REQUIRED** |
| _r2_ | Your Illumina Read 2 (R2) fastq.gz file that will be used for assembly. If you are running "single" read pilon mode, input your r1 file as a dummy for r2. **REQUIRED** |
| _ont_ | Your Oxford Nanopore reads fastq.gz file that will be used for assembly. If you want to run a "spades" only run mode, ont is not required. **REQUIRED for "full" run mode and "flye" run mode only.** |
| _index_ | Your packaged pankmer '.tar' archive containing the index files to be used for binning and a '.tsv' file with the reference genome sizes that are 90% of their total lengths. The reference genome sizes should have the same names as they are in your index. **REQUIRED** |
| _threads_ | The number of threads to use for mapping/assembly/etc. DEFAULT = 16 |
| _pilon_rounds_ | The number of polishing rounds you want to run with pilon. DEFAULT = 3 |
| _pilon_mode_ | The pilon mode that you want to run (paired or single). If you want to run "single" pilon mode, a r2 file is not required. DEFAULT = paired |
| _run_type_ | Specify the assembly path that you want to run (full, spades, flye). DEFAULT = full |
| _threshold_ | The threshold value used for binning. DEFAULT = 50 |


# Outputs
## Full workflow (SPAdes and Flye)
**Running the full workflow should produce:**
- {sample_name}_final_assembly.fasta    &nbsp; &nbsp;    _#final merged assembly_
- {sample_name}_pkbinned.tar.gz    &nbsp; &nbsp;    _#compressed folder containing binning results_
    - {sample_name}.full_ref_cov.html    &nbsp; &nbsp;    _#bar plot of reference coverage_
    - {sample_name}.full_{species}.fasta    &nbsp; &nbsp;    _#identified contigs for references_
    - {sample_name}.full_{species}.ONT.depth    &nbsp; &nbsp;    _#nanopore depth coverage_
    - {sample_name}.full_{species}.ILL.depth    &nbsp; &nbsp;    _#illumina depth coverage_
    - {sample_name}.full_{species}.top.topology.txt    &nbsp; &nbsp;    _#contig topology of identified references
    - {sample_name}.full_{species}.contig.cov    &nbsp; &nbsp;    _#contig coverage of references#_
- {sample_name}_ASM_work.tar.gz
    - flye/
        - assembly_info.txt &nbsp; &nbsp;   _#flye assembly contig information file_
        - {sample_name}_assembly.fasta   &nbsp; &nbsp;   _#flye assembly_
        - flye.pilon_finished{n}.fasta   &nbsp; &nbsp;   _#polished flye assembly_
        - flye.pilon_finished{1 to n}.vcf   &nbsp; &nbsp;    _#flye pilon variant output files for each round_
        - flye.pilon_finished{1 to n}.changes  &nbsp; &nbsp;  _#flye pilon changes files for each round_
    - spades/
        - assembly_info.tsv &nbsp; &nbsp;   _#spades assembly contig information file_
        - {sample_name}_assembly.fasta   &nbsp; &nbsp;   _#ONT context contigs_
        - {sample_name}_scaffolds.fasta  &nbsp; &nbsp;   _#spades assembly_
        - {sample_name}_spades_unmapped_contigs.fasta    &nbsp; &nbsp;   _#spades unmapped contigs_
        - spades.pilon_finished{n}.fasta    &nbsp; &nbsp;    _#polished ONT context contigs_
        - spades.pilon_finished{1 to n}.vcf  &nbsp; &nbsp;   _#spades pilon variant output files for each round_
        - spades.pilon_finished{1 to n}.changes  &nbsp; &nbsp;   _#spades pilon changes files for each round_


## Just SPAdes
**Running just the SPAdes assembly should produce:**
- {sample_name}_pkbinned.tar.gz    &nbsp; &nbsp;    _#compressed folder containing binning results_
    - {sample_name}.full_ref_cov.html    &nbsp; &nbsp;    _#bar plot of reference coverage_
    - {sample_name}.full_{species}.fasta    &nbsp; &nbsp;    _#identified contigs for references_
    - {sample_name}.full_{species}.ONT.depth    &nbsp; &nbsp;    _#nanopore depth coverage_
    - {sample_name}.full_{species}.ILL.depth    &nbsp; &nbsp;    _#illumina depth coverage_
    - {sample_name}.full_{species}.top.topology.txt    &nbsp; &nbsp;    _#contig topology of identified references
    - {sample_name}.full_{species}.contig.cov    &nbsp; &nbsp;    _#contig coverage of references#_ results_
- {sample_name}_ASM_work/
    - spades/
        - assembly_info.tsv &nbsp; &nbsp;   _#spades assembly contig information file_
        - {sample_name}_scaffolds.fasta  &nbsp; &nbsp;  _#spades assembly_


## Just Flye
**Running just the Flye assembly should produce:**
- {sample_name}_pkbinned.tar.gz    &nbsp; &nbsp;    _#compressed folder containing binning results_
    - {sample_name}.full_ref_cov.html    &nbsp; &nbsp;    _#bar plot of reference coverage_
    - {sample_name}.full_{species}.fasta    &nbsp; &nbsp;    _#identified contigs for references_
    - {sample_name}.full_{species}.ONT.depth    &nbsp; &nbsp;    _#nanopore depth coverage_
    - {sample_name}.full_{species}.ILL.depth    &nbsp; &nbsp;    _#illumina depth coverage_
    - {sample_name}.full_{species}.top.topology.txt    &nbsp; &nbsp;    _#contig topology of identified references
    - {sample_name}.full_{species}.contig.cov    &nbsp; &nbsp;    _#contig coverage of references#_
- {sample_name}_ASM_work/
    - flye/
        - assembly_info.txt &nbsp; &nbsp;   _#flye assembly contig information file_
        - flye.pilon_finished{n}.fasta  &nbsp; &nbsp; _#final polished flye assembly_
        - {sample_name}_assembly.fasta  &nbsp; &nbsp;  _#unpolished flye assembly_
        - flye.pilon_finished{1 to n}.vcf   &nbsp; &nbsp;  _#pilon variant output files for each round_
        - flye.pilon_finished{1 to n}.changes   &nbsp; &nbsp;  _#pilon changes files for each round_
