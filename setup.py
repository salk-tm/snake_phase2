from setuptools import setup

setup(
    name="phase2_scripts",
    version="0.1.5",
    packages=[],
    scripts=[
        "scripts/get_uncovered_contigs.py",
        "scripts/get_spades_context.py",
        "scripts/get_combined_fasta.py",
        "scripts/clean_spades_contigs.py",
        "scripts/get_assembly_info.py",
	    "scripts/rename_assembly_contigs.py"
    ]
)
