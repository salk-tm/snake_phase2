##### TE4 WORKFLOW #####

## Validation
from snakemake.utils import validate
import json
print(json.dumps(config, indent=4))
validate(config, "config.schema.json")
print(json.dumps(config, indent=4))


## Docker container
container: "docker://nolanhartwick/salk-containers:felix_assembly.V2"


## Configs
sample_name = config["sample"]
threads = config["threads"]
r1_reads = config["r1"]
r2_reads = config["r2"]
index = config["index"]
pilon_rounds = config["pilon_rounds"]
pilon_mode = config["pilon_mode"]
run_type = config["run_type"]
threshold = config["threshold"]
nanopore = config.get("ont", None)


## Binning input funtions
def binning_assembly_input():
    if (config['run_type'] == "spades"):
        return f"{sample_name}_ASM_work/spades/{sample_name}_scaffolds.fasta"
    elif (config['run_type'] == "flye"):
        return f"{sample_name}_ASM_work/flye/flye.pilon_finished" + f"{pilon_rounds}.fasta"
    elif (config['run_type'] == "full"):
        return f"{sample_name}_final_assembly.fasta"

def binning_info_input():
    if (config['run_type'] == "spades"):
        return f"{sample_name}_ASM_work/spades/assembly_info.tsv"
    elif (config['run_type'] == "flye"):
        return f"{sample_name}_ASM_work/flye/assembly_info.txt"
    elif (config['run_type'] == "full"):
        return "merged_assembly_info.tsv"

def binning_input():
    if (config['run_type'] == "spades"):
        return binning_assembly_input(), binning_info_input(), index, r1_reads, r2_reads
    else:
        return binning_assembly_input(), binning_info_input(), index, r1_reads, r2_reads, nanopore

## binning: binning to merged assembly
rule binning:
    input:
        binning_input()
    output:
        fastas = f"{sample_name}_pkbinned.tar.gz"
    params:
        threshold = threshold,
        name = sample_name,
        run = run_type,
        ont_opt = "" if config['run_type'] == "spades" else "../" + nanopore
    threads:
        threads
    shell:
        """
        ## Rename contigs in assembly

        rename_assembly_contigs.py {input[1]} {input[0]}
        rm {input[0]}
        mv renamed.fasta {input[0]}


        ## Compress assembly for pk_binning
        gzip {input[0]}


        ## Unpack index folder into a newly created folder
        if [ ! -e {params.name}_pkidx ]; then
            mkdir {params.name}_pkidx
            tar -xf {input[2]} -C {params.name}_pkidx
        elif [ ! -d {params.name}_pkidx ]; then
            echo "{params.name}_pkidx already exists" 1>&2
        fi

        ## Run pk_binning and package output folder
        pk_binning -a {input[0]}.gz -p {params.name} -o pkbinned \
            -i {params.name}_pkidx -s {params.name}_pkidx/*.tsv -c {threads} -x {params.threshold}


        ## Create topology files
        cd pkbinned/

        for file in $(ls *.fasta); do
            grep ">" ${{file}} | cut -f2 -d ">" | xargs -I {{}} grep -m 1 -w {{}} ../{input[1]} > ${{file}}.top
        done
        for file in $(ls *.top); do
            awk 'OFS="\t" {{if($5=="Y") {{print FILENAME,$2,"circular"}}else{{print FILENAME,$2,"linear"}}}}' ${{file}} | grep -v length > ${{file}}.topology.txt && sed -i 's/\.fasta\.top//g' ${{file}}.topology.txt
        done
        rm *.top
        
        ## Create Illumina depth files
        for file in $(ls *.fasta); do
            minimap2 -ax sr ${{file}} ../{input[3]} ../{input[4]} -t {threads} | \
                samtools sort -o ${{file}}.ILL.bam
            samtools index ${{file}}.ILL.bam
            samtools depth -aa ${{file}}.ILL.bam -o ${{file}}.ILL.depth
        done
        ##Calculates the coverage of Illumina reads per contig from the depth files
        for file in $(ls *.fasta); do 
            grep ">" ${{file}} | cut -f2 -d ">" > ${{file}}.contig.list
            for line in $(cat ${{file}}.contig.list); do 
                grep ${{line}} ${{file}}.ILL.depth > ${{file}}.${{line}}.depth.contig
            done
            for contig_file in $(ls ${{file}}*.depth.contig); do 
                awk '{{ sum += $3 }} END {{ if (NR > 0) print "ILL",FILENAME,sum / NR }}' ${{contig_file}} | \
                sed 's/\.depth\.contig//g' >> ${{file}}.contig.cov
            done
        done

        rm *.depth.contig
        rm *ILL.bam
        rm *ILL.bam.bai

        ## Create ONT depth files
        if [ {params.run} != "spades" ]; then
            for file in $(ls *.fasta); do
                minimap2 -ax map-ont ${{file}} {params.ont_opt} -t {threads} | \
                    samtools sort -o ${{file}}.ONT.bam
                samtools index ${{file}}.ONT.bam
                samtools depth -aa ${{file}}.ONT.bam -o ${{file}}.ONT.depth
            done

            ##Calculates the coverage of Nanopore reads per contig from the depth files
            for file in $(ls *.fasta); do
                for line in $(cat ${{file}}.contig.list); do 
                    grep ${{line}} ${{file}}.ONT.depth > ${{file}}.${{line}}.depth.contig
                done
                for contig_file in $(ls ${{file}}*.depth.contig); do 
                    awk '{{ sum += $3 }} END {{ if (NR > 0) print "ONT",FILENAME,sum / NR }}' ${{contig_file}} | \
                    sed 's/\.depth\.contig//g' >> ${{file}}.contig.cov
                done
            done


            rm *.depth.contig
            rm *ONT.bam
            rm *ONT.bam.bai
        fi

        rm *.contig.list

        cd ..

        sleep 10

        ## Pack binning results
        tar -czf {output.fastas} pkbinned/
        """


## merge: Combine polished Oxford reads, flye contigs, and spades contigs not covered by flye nor by Oxford reads
rule merge:
    input:
        unmapped = f"{sample_name}_ASM_work/spades/{sample_name}_spades_unmapped_contigs.fasta",
        context = f"{sample_name}_ASM_work/spades/spades.pilon_finished" + f"{pilon_rounds}.fasta",
        flye = f"{sample_name}_ASM_work/flye/flye.pilon_finished" + f"{pilon_rounds}.fasta",
        info = f"{sample_name}_ASM_work/flye/assembly_info.txt"
    output:
        f"{sample_name}_final_assembly.fasta",
        "merged_assembly_info.tsv"
    params:
        name = sample_name
    shell:
        """
        ## Remove contigs that are <500bp and >90% a single nucleotide
        clean_spades_contigs.py {input.unmapped} {input.context}

        ## Merge contigs
        get_combined_fasta.py --sample {params.name} --unmapped clean_unmapped.fasta \
            --context clean_context.fasta --flye {input.flye} --info {input.info} \
            --output {params.name}_final_assembly.fasta
        """


## pilon: Polish flye or spades assemblies
rule pilon:
    input:
        assembly = f"{sample_name}" + "_ASM_work/{some}/" + f"{sample_name}_assembly.fasta",
        r1 = r1_reads,
        r2 = r2_reads
    output:
        fasta = f"{sample_name}" + "_ASM_work/{some}/{some}.pilon_finished" + f"{pilon_rounds}.fasta",
        vcfs = [f"{sample_name}_ASM_work/{{some}}/{{some}}.pilon_finished{i}.vcf" for i in range(1, pilon_rounds+1)],
        changes = [f"{sample_name}_ASM_work/{{some}}/{{some}}.pilon_finished{i}.changes" for i in range(1, pilon_rounds+1)]
    params:
        pilon_mode = pilon_mode,
        name = sample_name,
        rounds = pilon_rounds
    threads:
        threads
    shell:
        """
        ## Check if file is empty
        if [ ! -s {input[0]} ]; then
            touch {output}
            exit 0
        fi

        ## Polish assembly, default 3 rounds
        current={input[0]}

        for round in {{1..{params.rounds}}};
        do
            bwa index ${{current}}

            if [ {params.pilon_mode} = "paired" ]; then
                minimap2 -ax sr -t {threads} ${{current}} {input[1]} {input[2]} | \
                    samtools sort -@ {threads} -o {params.name}_ASM_work/{wildcards.some}/illumina_to_asm.bam

                samtools index {params.name}_ASM_work/{wildcards.some}/illumina_to_asm.bam

                pilon -Xmx250g --genome ${{current}} \
                    --jumps {params.name}_ASM_work/{wildcards.some}/illumina_to_asm.bam \
                    --changes --verbose --nostrays --fix bases --vcf \
                    --output {params.name}_ASM_work/{wildcards.some}/{wildcards.some}.pilon_finished${{round}}

                sed -i 's/_pilon//g' {params.name}_ASM_work/{wildcards.some}/{wildcards.some}.pilon_finished${{round}}.*


            elif [ {params.pilon_mode} = "single" ]; then
                bwa mem ${{current}} {input[1]} | \
                    samtools view -b - > {params.name}_ASM_work/{wildcards.some}/illumina_to_asm.bam

                pilon --genome ${{current}} \
                    --frags {params.name}_ASM_work/{wildcards.some}/illumina_to_asm.bam --vcf \
                    --output {params.name}_ASM_work/{wildcards.some}/{wildcards.some}.pilon_finished${{round}}

                sed -i 's/_pilon//g' {params.name}_ASM_work/{wildcards.some}/{wildcards.some}.pilon_finished${{round}}.*
            fi

            current={params.name}_ASM_work/{wildcards.some}/{wildcards.some}.pilon_finished${{round}}.fasta
        done
        """
            

if config['run_type'] != 'spades':
    ## recover: Filter spades assembly with flye and oxford
    rule recover:
        input:
            flye = f"{sample_name}_ASM_work/flye/flye.pilon_finished" + f"{pilon_rounds}.fasta",
            scaffolds = f"{sample_name}_ASM_work/spades/{sample_name}_scaffolds.fasta",
            ont = nanopore
        output:
            f"{sample_name}_ASM_work/spades/{sample_name}_spades_unmapped_contigs.fasta",
            f"{sample_name}_ASM_work/spades/{sample_name}_assembly.fasta"  ## aka ONT_context_contigs.fasta
        params:
            name = sample_name
        shell:
            """
            ## Blast spades contigs against flye assembly. Remove spades contigs contained by flye contigs 
            blastn -query {input.scaffolds} -subject {input.flye} \
                -out {params.name}_ASM_work/spades/blast_results.xml -outfmt "6 std qlen slen"
            get_uncovered_contigs.py -b {params.name}_ASM_work/spades/blast_results.xml \
                -q {input.scaffolds} -o {params.name}_ASM_work/spades/spades_only.fasta
            
            ## Remove spades contigs contained by flye contigs.
            minimap2 --paf-no-hit -t {threads} -x map-ont -N 1 {input.ont} \
                {params.name}_ASM_work/spades/spades_only.fasta > {params.name}_ASM_work/spades/spades_only_ont.paf

            ## Get spades only contig ids that are covered by Oxford reads. Get Oxford reads' ids that contain spades' ids
            cat {params.name}_ASM_work/spades/spades_only_ont.paf | cut -f1,2,6,11 | \
                awk '{{if ($4/$2 >= 0.9){{print $0}}}}' | cut -f1 | sort | \
                uniq > {params.name}_ASM_work/spades/spades_mapped_ids.txt
            cat {params.name}_ASM_work/spades/spades_only_ont.paf | cut -f1,2,6,11 | \
                awk '{{if ($4/$2 >= 0.9){{print $0}}}}' | cut -f3 | sort | \
                uniq > {params.name}_ASM_work/spades/ONT_spades_ids.txt

            ## Get Oxford reads that contain spades contigs and spades contigs that are not contained by Oxford read 
            get_spades_context.py --spades_ids {params.name}_ASM_work/spades/spades_mapped_ids.txt \
                --ONT_ids {params.name}_ASM_work/spades/ONT_spades_ids.txt \
                --spades_file {params.name}_ASM_work/spades/spades_only.fasta --ONT_file {input.ont}  \
                --spades_output {params.name}_ASM_work/spades/{params.name}_spades_unmapped_contigs.fasta \
                --ont_output {params.name}_ASM_work/spades/{params.name}_assembly.fasta
            """    
      

def spades_input():
    if (config['run_type'] == "spades"):
        return r1_reads, r2_reads
    else:
        return r1_reads, r2_reads, nanopore

## spades: Run spades
rule spades:
    input:
        spades_input()
    output:
        f"{sample_name}_ASM_work/spades/{sample_name}_scaffolds.fasta",
        f"{sample_name}_ASM_work/spades/assembly_info.tsv"
    params:
        threads,
        name = sample_name,
        ont_opt = "" if config['run_type'] == "spades" else "--nanopore " + nanopore
    shell:
        """
        ## Run spades assembly
        spades.py -1 {input[0]} -2 {input[1]} {params.ont_opt}\
                -t {threads} -o {params.name}_ASM_work/spades --meta

        ## Create info file for spades only run
        get_assembly_info.py \
            {params.name}_ASM_work/spades/scaffolds.fasta {params.name} spades

        ## Rename spades assembly with sample
        mv {params.name}_ASM_work/spades/scaffolds.fasta \
            {params.name}_ASM_work/spades/{params.name}_scaffolds.fasta
        """


if config['run_type'] != 'spades':
    ## flye: Run flye only if run_type does not equal "spades"
    rule flye:
        input:
            ont = nanopore
        output:
            f"{sample_name}_ASM_work/flye/{sample_name}_assembly.fasta",
            f"{sample_name}_ASM_work/flye/assembly_info.txt"
        params:
            name = sample_name,
            run = run_type
        threads:
            threads
        shell:
            """
            ## Run flye assembly
            flye --nano-raw {input.ont} -t {threads} \
                --plasmids --meta -o {params.name}_ASM_work/flye

            ## Create info file for flye only run
            if [ {params.run} = "flye" ]; then
                get_assembly_info.py \
                    {params.name}_ASM_work/flye/assembly_info.txt {params.name} flye
                rm {params.name}_ASM_work/flye/assembly_info.txt
                mv {params.name}_ASM_work/flye/assembly_info.tsv {params.name}_ASM_work/flye/assembly_info.txt
            fi

            ## Rename flye assembly with sample
            mv {params.name}_ASM_work/flye/assembly.fasta \
                {params.name}_ASM_work/flye/{params.name}_assembly.fasta
            """