#!/usr/bin/env python3
import sys
import pandas as pd
from Bio import SeqIO


def spades_info(file, sample):
    df = pd.DataFrame(columns=['#seq_name', 'length', 'cov.', 'circ.', 'repeat', 'mult.', 'alt_group', 'graph_path'])

    for record in SeqIO.parse(file, "fasta"):
        rec = record.id
        name = sample + "_spades_" + rec.split('_')[1] + "_" + rec.split('_')[3]
        df.loc[len(df.index)] = [name, rec.split('_')[3], rec.split('_')[5], '', 'N', '', '', ''] 
    
    out = sample + '_ASM_work/spades/assembly_info.tsv'
    df.to_csv(out, sep='\t')
    return

def flye_info(file, sample):
    df = pd.read_csv(file, sep='\t')
    df = df.sort_values(by=['#seq_name'])
    df['temp'] = sample + "_flye_" + df['#seq_name'].str.split('_').str[1] + "_" + df['length'].astype(str)
    df['#seq_name'] = df['temp']
    del df['temp']

    out = sample + '_ASM_work/flye/assembly_info.tsv'
    df.to_csv(out, sep='\t')
    return

def main():
    file = sys.argv[1]
    sample = sys.argv[2]
    run = sys.argv[3]

    if run == 'spades':
        spades_info(file, sample)
    elif run == 'flye':
        flye_info(file, sample)


if __name__ == "__main__":
    main()