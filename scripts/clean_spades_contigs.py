#!/usr/bin/env python3
import sys
from Bio import SeqIO


def clean_seqs(file):
    input_seq_iterator = SeqIO.parse(file, 'fasta')

    clean_seqs = [record for record in input_seq_iterator \
    if (record.seq.count('A') / len(record.seq) < 0.9) and \
        (record.seq.count('C') / len(record.seq) < 0.9) and \
            (record.seq.count('T') / len(record.seq) < 0.9) and \
                (record.seq.count('G') / len(record.seq) < 0.9) and \
                    (len(record.seq) >= 500)]
    
    return clean_seqs

def main():
    unmapped_file = sys.argv[1]
    context_file = sys.argv[2]

    clean_unmapped = clean_seqs(unmapped_file)
    clean_context = clean_seqs(context_file)

    SeqIO.write(clean_unmapped, "clean_unmapped.fasta", "fasta")
    SeqIO.write(clean_context, "clean_context.fasta", "fasta")

if __name__ == "__main__":
    main()