#!/usr/bin/env python3

import sys
import gzip
import pandas as pd
from Bio import SeqIO


info = sys.argv[1]
assembly = sys.argv[2]


df = pd.read_csv(info, sep='\t')
names = df['#seq_name'].tolist()
i = 0

with open(assembly) as original, open("renamed.fasta",
'w') as renamed:
    records = SeqIO.parse(assembly, 'fasta')

    for record in records:
        record.id = names[i]
        record.description = names[i]
        i += 1

        SeqIO.write(record, renamed, 'fasta')